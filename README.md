[![pipeline status](https://gitlab.com/davidbittner/silkroad-integration/badges/master/pipeline.svg)](https://gitlab.com/davidbittner/silkroad-integration/commits/master)
# SilkRoad integration

The main project responsible for the SilkRoad integration at Unanet Technologies.

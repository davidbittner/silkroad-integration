package unanet.integrations;

import org.apache.commons.cli.*;
import org.yaml.snakeyaml.Yaml;
import unanet.integrations.silkroad.SilkRoad;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.*;

public class Handler {
    final static Logger logger = Logger.getGlobal();

    public static void main(String []args) {
        Formatter log_formatter = new Formatter() {
            @Override
            public String format(LogRecord record) {
                StringBuilder text = new StringBuilder();

                if(record.getLevel().intValue() <= Level.INFO.intValue()) {
                    text.append(record.getLevel().toString());
                    text.append(" ");
                    text.append(LocalDateTime.now().toString());
                    text.append(" : ");
                }

                text.append(record.getMessage());
                text.append("\n");

                return text.toString();
            }
        };

        ConsoleHandler handler = new ConsoleHandler();

        logger.setUseParentHandlers(false);
        logger.setLevel(Level.ALL);

        handler.setLevel(Level.ALL);
        handler.setFormatter(log_formatter);

        logger.addHandler(handler);

        Options options = new Options();
        options.addOption("p", true, "the location of the property file");
        options.addOption("o", true, "the name of the output file");
        options.addOption("l", true, "where to write the log file");
        options.addOption("w", true, "which API to access");

        options.addOption("h", "help", false, "print this help message");

        CommandLineParser arg_parser = new DefaultParser();

        CommandLine cmd;
        try{
            cmd = arg_parser.parse(options, args);
        }catch(ParseException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("integrator", options);

            return;
        }
        if(cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("integrator", options);

            return;
        }else if(cmd.hasOption("l")) {
            try{
                FileHandler log_to = new FileHandler(cmd.getOptionValue("l"));
                logger.addHandler(log_to);

                log_to.setLevel(Level.ALL);

                log_to.setFormatter(log_formatter);
            } catch(IOException ex) {
                logger.severe(ex.getLocalizedMessage());
            }
        }

        String output_file = cmd.getOptionValue("o", "output_data.csv");
        FileWriter output_writer;
        try{
            output_writer = new FileWriter(output_file);
        }catch (IOException ex) {
            logger.severe(ex.getLocalizedMessage());
            return;
        }

        FileReader prop_file;

        try{
            prop_file = new FileReader(cmd.getOptionValue("p", "config.yaml"));
        }catch(FileNotFoundException ex) {
            logger.severe("Unable to open credentials file: " + ex.getLocalizedMessage());
            return;
        }

        Yaml config_reader = new Yaml();
        @SuppressWarnings("unchecked")
        Map<String, Object> config = (Map<String, Object>)config_reader.load(prop_file);

        //If there is no command line value, use the config, if no config, use silkroad.
        String which = cmd.getOptionValue("w", (String)config.getOrDefault("which", "silkroad"));
        @SuppressWarnings("unchecked")
        Map<String, Object> focused_config = (Map<String, Object>)config.get(which);

        switch(which) {
            case "silkroad": {
                SilkRoad.Query(focused_config, output_writer);
            }
        }
    }
}

package unanet.integrations.silkroad.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "EpsTableEx")
public class EmployeeResponse {

    @Element(name = "ErrorNum", required = false)
    private int errorNum;

    @Element(name = "ErrorString", required = false)
    private String errorString;

    @Path("Data/ArrayOfString[1]")
    @ElementList(name = "ArrayOfString", inline = true)
    private ArrayList<String> headers;

    @Path("Data/ArrayOfString[2]")
    @ElementList(name = "ArrayOfString", inline = true)
    private ArrayList<String> data;

    public boolean wasError() {
        return errorNum != 1;
    }

    public String getErrorString() {
        return errorString;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        for(int i = 0; i < headers.size(); i++) {
            ret.append(headers.get(i));
            ret.append(" : ");
            ret.append(data.get(i));
            ret.append("\n");
        }
        return ret.toString();
    }

    /**
     * This strips identifiable information such as the SSN that we don't want visible in the output.
     */
    public void stripIdentifiableInformation(List<String> exclude_list, List<String> replace_list) {
        if(replace_list != null && exclude_list.size() != replace_list.size()) {
            throw new RuntimeException("Replacer list does not match the length of the exclude list");
        }

        for(String header : exclude_list) {
            int pos = headers.indexOf(header);
            if(pos != -1) {
                String replace = "";
                if(replace_list != null) {
                    replace = replace_list.get(exclude_list.indexOf(header));
                }

                data.set(pos, replace);
            }else{
                throw new RuntimeException(String.format("Failed to strip header '%s' as it doesn't exist.", header));
            }
        }
    }

    public ArrayList<String> getHeaders() {
        return headers;
    }

    /**
     * This returns the internal employee data.
     * @return A list of the data corresponding to each header passed earlier.
     */
    public ArrayList<String> getData() {
        ArrayList<String> ret = new ArrayList<>(data);

        for(int i = 0; i < ret.size(); i++) {
            if(ret.get(i).equals("\t")) {
                ret.set(i, "");
            }
        }

        return ret;
    }
}

package unanet.integrations.silkroad.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "GetUserIDListInput")
public class UserIDListRequest {
    @Element(name = "SynchronizationToken")
    private String syncToken;

    public UserIDListRequest(String token) {
        syncToken = token;
    }
}

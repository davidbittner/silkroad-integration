package unanet.integrations.silkroad.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root
@Namespace(reference = "http://Eprise")
public class EmployeeXML {

    @Element(name = "ErrorString")
    String errorString;

    @Element(name = "ErrorNum")
    int errorNum;

    @Element(name = "Data")
    String innerXml;

    public String getXML() {
        return innerXml;
    }

    @Override
    public String toString() {
        return errorString + "\n" +
                errorNum + "\n" +
                innerXml;
    }
}
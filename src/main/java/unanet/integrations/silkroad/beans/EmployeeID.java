package unanet.integrations.silkroad.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "User")
public class EmployeeID {
    @Element(name = "Guid")
    String guid;

    @Element(name = "Employee_HRISID", required = false)
    String empHrisid;

    @Element(name = "SSOAuthParam", required = false)
    String ssoParam;

    @Element(name = "LoginID")
    String loginId;

    @Element(name = "Email")
    String email;

    @Element(name = "LifeSuiteID", required = false)
    String lsId;

    @Element(name = "EmployeeStatus")
    String status;

    @Element(name = "SynchronizationToken")
    String syncToken;

    String getSyncToken() {
        return syncToken;
    }

    public String getLoginID() {
        return loginId;
    }

    @Override
    public String toString() {
        return guid + " " + email;
    }
}

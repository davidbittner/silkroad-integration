package unanet.integrations.silkroad.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root
public class EmployeeIDList {
    @ElementList(name = "Users")
    List<EmployeeID> users;

    @Element(name = "ErrorString", required = false)
    private String errorString;

    @Element(name = "ErrorNum", required = false)
    private int errorNum;

    public List<EmployeeID> getList() {
        return users;
    }

    public String getLastSyncToken() {
        return users.get(users.size()-1).getSyncToken();
    }

    public boolean wasError() {
        return errorString != null;
    }

    public String errorString() {
        return errorString;
    }
}
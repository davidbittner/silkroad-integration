package unanet.integrations.silkroad;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import unanet.integrations.silkroad.beans.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import static unanet.integrations.utils.WebUtilities.getResponseText;
import static unanet.integrations.utils.WebUtilities.makeRequest;

class WebHandler {
    private static Logger logger = Logger.getGlobal();

    private String sessionID;
    private String api_url;

    /**
     * The constructor that establishes the initial authentication handshake.
     * @param user The username for authentication.
     * @param pass The password for authentication.
     * @param api_url The URL to make the auth request to.
     */
    WebHandler(String user, String pass, String api_url) {
        this.api_url = api_url;

        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("strLogInId",  user);
        parameters.put("strPassword", pass);

        HttpURLConnection response = makeRequest(api_url + "/LogIn", parameters);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(response.getInputStream());

            //Weird, I know
            sessionID = doc.getDocumentElement().getFirstChild().getNodeValue();
        } catch (ParserConfigurationException | SAXException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        } catch (IOException e) {
            throw new RuntimeException("Failed to read from response buffer.");
        }

        if(sessionID.isEmpty()) {
            throw new RuntimeException("Invalid credentials supplied.");
        }
    }

    /**
     * This closes the session to invalidate the session ID.
     */
    void closeSession() {
        if(sessionID.isEmpty()) {
            logger.info("Session ID is blank, therefore session was not established. Quitting.");
        }else{
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("strSessionNum", sessionID);

            //Logs out, response not needed. If it failed, then the sessionID wasn't valid anyway.
            //Response text is retrieved to be sure the request itself is actually made.
            //I am not sure whether or not java evaluates URL requests lazily.
            getResponseText(makeRequest(api_url + "/Logout", parameters));
            sessionID = "";
        }
    }

    /**
     * This function requests all of the employee IDs from the API and serializes them internally.
     * @return Returns the list of the employees.
     */
    ArrayList<EmployeeID> requestIDs() {
        final int MAX_EMPLOYEES = 1000;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("strSecurityToken", sessionID);

        EmployeeIDList cur_list;
        ArrayList<EmployeeID> return_list = new ArrayList<>();

        UserIDListRequest request = new UserIDListRequest("");
        Serializer   serializer   = new Persister();

        do{
            //The serializer for writing the XML of the request
            StringWriter serialize_to = new StringWriter();

            //Attempt to serialize the request XML
            try{
                serializer.write(request, serialize_to);
            }catch(Exception ex) {
                throw new RuntimeException(ex.getLocalizedMessage());
            }

            //Put the XML as a query parameter
            parameters.put("strXML", serialize_to.toString());
            HttpURLConnection response = makeRequest(api_url + "/GetUserIDList", parameters);

            //Read the XML returned and serialize it into a list of employees
            try{
                EmployeeXML xml = serializer.read(EmployeeXML.class, response.getInputStream());
                cur_list        = serializer.read(EmployeeIDList.class, xml.getXML());
            }catch(Exception ex) {
                throw new RuntimeException(ex.getLocalizedMessage());
            }

            if(cur_list.wasError()) {
                throw new RuntimeException(cur_list.errorString());
            }

            //Add it to the total list and continue while there are 1000 employees being returned
            //It will only return 1000 employees at once, so you call using the last sync token until
            //there are no more employees left.
            return_list.addAll(cur_list.getList());

            if(cur_list.getList().size() > 0) {
                request = new UserIDListRequest(cur_list.getLastSyncToken());
            }
        }while(cur_list.getList().size() == MAX_EMPLOYEES);

        return return_list;
    }

    /**
     * This function takes a list of IDs and gets further information about each individual employee.
     * @param ids The list of IDs to query the API for.
     * @return The information for each employee.
     */
    ArrayList<EmployeeResponse> getEmployeeInformation(ArrayList<EmployeeID> ids) {
        //This will later loop through the IDs parameter, making a request for each employee.
        ArrayList<EmployeeResponse> ret = new ArrayList<>();

        Serializer deserializer = new Persister();

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("strSecurityToken", sessionID);
        parameters.put("strLoginID", "Web.Services");

        HttpURLConnection response = makeRequest(api_url + "/GetUserProfile", parameters);
        EmployeeResponse ser_resp = new EmployeeResponse();

        try{
            StringWriter temp = new StringWriter();
            temp.write(getResponseText(response));

            deserializer.read(ser_resp, temp.toString().replaceAll("<string/>", "<string>\t</string>"));

            ret.add(ser_resp);
        }catch(Exception ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }

        return ret;
    }
}

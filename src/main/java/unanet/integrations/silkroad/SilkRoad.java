package unanet.integrations.silkroad;

import com.opencsv.CSVWriter;
import org.apache.commons.lang3.ObjectUtils;
import unanet.integrations.silkroad.beans.EmployeeID;
import unanet.integrations.silkroad.beans.EmployeeResponse;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

/**
 * The entry point to the program.
 */

public class SilkRoad {
    private static Logger logger = Logger.getGlobal();

    private static String getField(Map<String, Object> config, String field_name ) {
        try {
            return config.get(field_name).toString();
        }catch(NullPointerException ex) {
            logger.severe(String.format("missing '%s' config field in silkroad block", field_name));
            return null;
        }
    }

    public static void Query(Map<String, Object> config, FileWriter write_to) {
        String user, pass, silkroad_url;

        user         = getField(config, "user");
        pass         = getField(config, "pass");
        silkroad_url = getField(config, "url");

        if(pass == null || user == null || silkroad_url == null) {
            return;
        }

        WebHandler auth_handler;

        try{
            auth_handler = new WebHandler(user, pass, silkroad_url);
        }catch(RuntimeException ex) {
            logger.severe("Failed during attempted log-in with message:");
            logger.severe("\t" + ex.getLocalizedMessage());
            return;
        }

        ArrayList<EmployeeID> employee_list;

        try{
            //employee_list = auth_handler.requestIDs(silkroad_url);
        }catch(RuntimeException ex) {
            logger.severe("Failed during attempted retrieval of employee IDs with message:");
            logger.severe("\t" + ex.getLocalizedMessage());
            auth_handler.closeSession();
            return;
        }

        ArrayList<EmployeeResponse> employee_data;
        try{
            employee_data = auth_handler.getEmployeeInformation(null);
        }catch(RuntimeException ex) {
            logger.severe("Error occurred while getting employee information with the message:");
            logger.severe("\t" + ex.getLocalizedMessage());
            auth_handler.closeSession();
            return;
        }

        boolean strip = (boolean)config.get("exclude");

        if(strip) {
            List<String> list      = (List<String>)config.get("excludes");
            List<String> replacers = (List<String>)config.get("replace");

            if(list.size() > 0) {
                for(EmployeeResponse employee : employee_data) {
                    employee.stripIdentifiableInformation(list, replacers);
                }
            }
        }

        CSVWriter writer;

        writer = new CSVWriter(write_to);

        if(employee_data.size() == 0) {
            logger.severe("Employee data list is empty.");
            return;
        }else{
            //Write headers
            writer.writeNext(employee_data.get(0).getHeaders().toArray(new String[0]));
            for(EmployeeResponse emp : employee_data) {
                writer.writeNext(emp.getData().toArray(new String[0]));
            }
        }

        try{
            writer.close();
        }catch(IOException ex) {
            logger.severe("Failed to close writer:");
            logger.severe("\t" + ex.getLocalizedMessage());
        }finally{
            auth_handler.closeSession();
        }
    }
}

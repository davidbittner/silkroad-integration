package unanet.integrations.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class WebUtilities {

    /**
     * Generates a parameter string from the given parameters. For example,
     * A = 15
     * B = 12
     * Becomes: A=15&B=12
     *
     * @param params The parameters you want to be put into the parameter string.
     * @return The parameter string itself.
     */
    private static String parameterString(HashMap<String, String> params) {
        StringBuilder ret = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            try {
                String key = URLEncoder.encode(entry.getKey(), "UTF-8");
                String value = URLEncoder.encode(entry.getValue(), "UTF-8");

                ret.append(key);
                ret.append("=");
                ret.append(value);
                ret.append("&");

            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex.getLocalizedMessage());
            }
        }

        String return_string = ret.toString();
        return (return_string.isEmpty()) ? (return_string) : (return_string.substring(0, return_string.length() - 1));
    }

    /**
     * A helper function to make a request to a given URL. It takes a hashmap of parameters.
     *
     * @param url        The URL to make the request to.
     * @param parameters The parameters to append to the URL.
     * @return The connection to the URL which is a generic read interface.
     */
    public static HttpURLConnection makeRequest(String url, HashMap<String, String> parameters) {
        URL request_url;

        String queryString = parameterString(parameters);

        try {
            //Verifies that the URL is actually valid.
            URI resolver = new URI(url + "?" + queryString);
            request_url = new URL(resolver.toString());
        } catch (MalformedURLException ex) {
            throw new RuntimeException(String.format("'%s' is not a valid URL.", url));
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }

        HttpURLConnection conn;
        try {
            conn = (HttpURLConnection) request_url.openConnection();
            conn.setRequestMethod("GET");
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }

        return conn;
    }

    /**
     * This takes a URL connection and turns the response into a string. This really is just a helper function.
     *
     * @param resp The response from a HTTP request.
     * @return The string that the response returned.
     */
    public static String getResponseText(HttpURLConnection resp) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resp.getInputStream()));

            StringBuilder ret = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                ret.append(line + "\n");
            }

            reader.close();
            resp.disconnect();

            return ret.toString();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }
}
